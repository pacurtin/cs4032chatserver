class ChatServer

  require 'thread'
  class ThreadPool
    def initialize(max_size)
      @pool = []
      @max_size = max_size
      @pool_mutex = Mutex.new
      @pool_cv = ConditionVariable.new
    end

    def dispatch(*args)
      Thread.new do
        # Wait for space in the pool.
        @pool_mutex.synchronize do
          while @pool.size >= @max_size
            print "Pool is full; waiting to run #{args.join(',')}...\n" if $DEBUG
            # Sleep until some other thread calls @pool_cv.signal.
            @pool_cv.wait(@pool_mutex)
          end
        end
        @pool << Thread.current
        begin
          yield(*args)
        rescue => e
          exception(self, e, *args)
        ensure
          @pool_mutex.synchronize do
            # Remove the thread from the pool.
            @pool.delete(Thread.current)
            # Signal the next waiting thread that there's a space in the pool.
            @pool_cv.signal
          end
        end
      end
    end
    def shutdown
      @pool_mutex.synchronize { @pool_cv.wait(@pool_mutex) until @pool.empty? }
    end
    def exception(thread, exception, *original_args)
      # Subclass this method to handle an exception within a thread.
      puts "Exception in thread #{thread}: #{exception}"
    end
  end


  class ChatRoom
    attr_accessor :members, :roomName, :roomRef

    def initialize(roomName)
      @members =[]
      @roomName =roomName
      @roomRef
    end

  end


#MAIN Loop
  pool=ThreadPool.new(10)
  roomRef=0
  joinID=0
  chatRoomArray= Array.new {ChatRoom.new(roomName)}
  
  #Some lines for Debugging.
  #chatRoomArray[0]=ChatRoom.new(roomName)
  #chatRoomArray[0].members<<"Jim"
  #chatRoomArray[0].members<<"Mary"
  #puts "#{chatRoomArray[0].roomName}"
  #puts "#{chatRoomArray[0].members}"


  receivedMessage=''     #empty string used to store and concatenate lines sent from client.

  require 'socket'                # Get sockets from stdlib
  server = TCPServer.open(8000)   # Socket to listen on port 8000


  loop {                          # Servers run forever
    Thread.start(server.accept) do |client|
      pool.dispatch(client) do |client|

        while line = client.gets.chomp   # Read line from the socket

          receivedMessage=receivedMessage+"#{line}"	#Concatenating message from client
		puts receivedMessage

		#This section of code kills server
	  if receivedMessage=='KILL_SERVER'
            client.puts "Server Down\n"		#message to terminal
            exit				#kill server

		#This section of code acts as echo server
          elsif receivedMessage.include?("HELO")
		client.puts "#{line}\nIP:#{Socket.gethostname}\nPort:8000\nStudentID:10323175\n"            
		receivedMessage=''		#clear recieved message for future messages


		#This section of code deals with join chatroom requests
          elsif (receivedMessage.start_with? 'JOIN_CHATROOM:') && receivedMessage.include?('CLIENT_NAME:')

              chatRoomExisted=0
              counter=0
              chatRoomName=receivedMessage[14,receivedMessage.index('CLIENT_IP:').to_i-14]

              while counter<chatRoomArray.size
                if chatRoomArray[counter].roomName==chatRoomName
                  chatRoomArray[counter].members << client           #if chatroom exists add client to it...
                  #puts "Existing rooms members: "+"#{chatRoomArray[counter].members}"
                  chatRoomExisted=1

                  roomMembers=chatRoomArray[counter].members
                  clientName=receivedMessage[receivedMessage.index('CLIENT_NAME:').to_i+12,receivedMessage.size]
			server_ip=IPSocket.getaddress(Socket.gethostname)
                  send="JOINED_CHATROOM: "+"#{chatRoomName}" + "\n"+ 'SERVER_IP: '+ "#{server_ip}" "\n" 'PORT: 0'+ 			  "\n"+"ROOM_REF: "+"#{chatRoomArray[counter].roomRef}"+ "\n"+ "JOIN_ID: "  +"#{joinID}"
		  client.puts send        #Send message
                  joinID=joinID+1

                  membersCount=0
                  while membersCount<roomMembers.size
                    roomMembers[membersCount].puts "#{clientName}"+" has joined room"+chatRoomName
                    membersCount=membersCount+1
                  end
                end
                counter = counter + 1
              end

              if chatRoomExisted==0	#Chatroom didn't exist
                chatRoomArray << ChatRoom.new(chatRoomName)
                chatRoomArray[counter].members << client
                chatRoomArray[counter].roomRef = roomRef          #...else create new room and add client to it

                #puts "New rooms members: "+"#{chatRoomArray[counter].members}"
                clientName=receivedMessage[receivedMessage.index('CLIENT_NAME:').to_i+12,receivedMessage.size]

		server_ip=IPSocket.getaddress(Socket.gethostname)
                send="JOINED_CHATROOM: "+"#{chatRoomName}" + "\n"+ "SERVER_IP: "+ "#{server_ip}" + "\n" 'PORT: 0'+ 			"\n"+ "ROOM_REF: "  +"#{roomRef}"+ "\n"+ "JOIN_ID: "  +"#{joinID}"
                client.puts send        #Send message
                
		message="#{clientName}"+" has joined this chatroom."
		send="CHAT: "+"#{roomRef}" + "\n"+ 'CLIENT_NAME: '+ "#{clientName}" + "\n" + 			"MESSAGE: "  +"#{message}"
		client.puts send
		roomRef=roomRef+1
                joinID=joinID+1
              end

              receivedMessage=""


		#This section of code deals with leave chatroom requests
          elsif (receivedMessage.start_with? 'LEAVE_CHATROOM:') && receivedMessage.include?('CLIENT_NAME:')

            clientNameReceived=receivedMessage[receivedMessage.index('CLIENT_NAME:').to_i+12,receivedMessage.size]
            chunkSize=(clientNameReceived.size)+13
            reducedMessage=receivedMessage[0...-chunkSize]
            joinIDReceived=reducedMessage[reducedMessage.index('JOIN_ID:').to_i+9,reducedMessage.size]
            chunkSize=(joinIDReceived.size)+9
            reducedMessage=reducedMessage[0...-chunkSize]
            roomRefReceived=reducedMessage[16,reducedMessage.size]

		puts "Chatrooms: "+ "#{chatRoomArray.size}"+"\n"
            leaveRoom=0
            i=0
            while i<chatRoomArray.size
              if chatRoomArray[i].roomRef=roomRefReceived
                leaveRoom=i
                i=i+1
              end
            end

            i=i-1
            leaveRoomMembers=chatRoomArray[i].members
            puts 'before'
            puts leaveRoomMembers


            leaveRoomMembers.delete(client)
            puts 'after'
            puts leaveRoomMembers


            chatRoomArray[i].members=leaveRoomMembers


            receivedMessage=''
            reducedMessage=''


		#This section of code deals with messages
          elsif (receivedMessage.start_with? 'CHAT:') && receivedMessage.include?('MESSAGE: ')


            roomRefReceived=receivedMessage[6,receivedMessage.index('JOIN_ID:').to_i-6]
            message=receivedMessage[receivedMessage.index('MESSAGE:').to_i+9,receivedMessage.size]
            chunkSize=(message.size)+9
            reducedMessage=receivedMessage[0...-chunkSize]
            clientNameReceived=reducedMessage[reducedMessage.index('CLIENT_NAME:').to_i+13,reducedMessage.size]
            chunkSize=(clientNameReceived.size)+13
            reducedMessage=reducedMessage[0...-chunkSize]
            joinIDReceived=reducedMessage[reducedMessage.index('JOIN_ID:').to_i+9,reducedMessage.size]

	    #Debug stuff...
            #puts roomRefReceived
            #puts joinIDReceived
            #puts clientNameReceived
            #puts message


            correctRoom=0
            i=0
            while i<chatRoomArray.size
              if chatRoomArray[i].roomRef=roomRefReceived
                correctRoom=i
                i=i+1
              end
            end

            i=i-1
            correctRoomMembers=chatRoomArray[i].members
            puts correctRoomMembers

            j=0
            while j<correctRoomMembers.size
              client=correctRoomMembers[j]
              send="CHAT: "+"#{roomRefReceived}" + "\n"+ 'CLIENT_NAME: '+ "#{clientNameReceived}" + "\n" + "MESSAGE: 			"  +"#{message}"
              client.puts send
              j=j+1
            end


            receivedMessage=''
            reducedMessage=''

	
	elsif(receivedMessage.start_with? 'JOIN_CHATROOM:')         		
	elsif(receivedMessage.start_with? 'LEAVE_CHATROOM:') #These allow the loop to concatenate messages  
	elsif(receivedMessage.start_with? 'CHAT:')           #without going to the else statement and deleting
 							     #what's already been read in.

	else
		receivedMessage=''	#Deals with garbage messages		
	end

		#More debugging stuff. Ignore it.		
		#puts "Recieved message is: "+"#{receivedMessage}"
          	#client.puts client     #Remember this is a thing
           	#client.puts "#{chatRoomArray[0].members}"   #Just need to load members into another array if 			individuals need to be accessed.
           	#client.puts "#{chatRoomArray[0].roomName}" + '\n\n'    #for dealing with \n\n delimiter
	
        end
      end
    end
  }


end
