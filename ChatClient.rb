class ChatClient

  require 'socket'      # Sockets are in standard library

  hostname = 'localhost'
  port = 8000

  s = TCPSocket.open(hostname, port)

  Thread.new do
    while line = s.gets   # Read lines from the socket
      puts line.chop      # Prints what was recieved from socket
      break if line.include? '\n\n'
    end
  end


  loop{

      state='a'
      puts "Press a to join a server, b to disconnect, c to send a message, d to kill server, e for student details.\n"
      state=gets.chomp

      if state=='a'
        puts 'JOIN_CHATROOM:'
        #chatRoomName=gets.chomp
        chatRoomName='Room 1'
        puts 'CLIENT_NAME:'
        #clientHandle=gets.chomp
        clientHandle='Paddy'
        send="JOIN_CHATROOM: "+"#{chatRoomName}" + "\n"+ 'CLIENT_IP: 0' + "\n" + 'PORT: 0'+ "\n"+ "CLIENT_NAME: "  +"#{clientHandle}"   #User input
        s.puts send        #Send message


      elsif state=='b'
        puts 'LEAVE_CHATROOM:'
        #roomRef=gets.chomp
        roomRef='0'
        puts 'JOIN_ID:'
        #joinID=gets.chomp
        joinID='0'
        puts 'CLIENT_NAME:'
        #clientHandle=gets.chomp
        clientHandle='Paddy'
        send="LEAVE_CHATROOM: "+"#{roomRef}"+ "\n" + "JOIN_ID: "  +"#{joinID}" + "\n" + "CLIENT_NAME: "  +"#{clientHandle}"  #User input
        s.puts send        #Send message


      elsif state=='c'
        puts 'CHAT:'
        #roomRef=gets.chomp
        roomRef='aaaa'
        puts 'JOIN_ID:'
        #joinID=gets.chomp
        joinID='bbbb'
        puts 'CLIENT_NAME:'
        #clientHandle=gets.chomp
        clientHandle='cccc'
        puts 'MESSAGE:'
        #message=gets.chomp + '\n\n'
        message='dddd'
        send="CHAT: "+"#{roomRef}"+ "\n" + "JOIN_ID: "  +"#{joinID}"+ "\n" + "CLIENT_NAME: "  +"#{clientHandle}"+ "\n" + "MESSAGE: "  +"#{message}"   #User input
        s.puts send        #Send message


      elsif state=='d'
        send="KILL_SERVER"
        s.puts send


      elsif state=='e'
        send="HELO"
        s.puts send


      else
        puts "incorrect selection\n"

      end
    }


end